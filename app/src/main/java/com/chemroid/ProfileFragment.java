package com.chemroid;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class ProfileFragment extends Fragment {
    Button btn_logout;
    LinearLayout membership, library, faq, about, setting;
    ImageView profilephoto;
    TextView name;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        btn_logout = view.findViewById(R.id.btn_logout);
        membership = view.findViewById(R.id.membership_lin);
        library = view.findViewById(R.id.library_lin);
        faq = view.findViewById(R.id.faq_lin);
        about = view.findViewById(R.id.about_lin);
        setting = view.findViewById(R.id.setting_lin);
        profilephoto = view.findViewById(R.id.profile_photo);
        name = view.findViewById(R.id.name);

        Logout();

        return view;
    }


    private  void Logout () {
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FirebaseAuth.getInstance().signOut();
                Intent intToMain = new Intent (getActivity(), Login.class);
                startActivity(intToMain);
            }
        });
    }
}
